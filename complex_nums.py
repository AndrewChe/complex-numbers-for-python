class Complex:
    a = 0.0
    b = 0.0
    def __init__ (self, x, y):
        self.a = x
        self.b = y

    def Re (self):
        return self.a

    def Im (self):
        return self.b

    def conjugate (self):
        return Complex (self.a, -self.b)

    def __str__ (self):
        if self.b >= 0:
            q = '+'
        else:
            q = ''
        return str (self.a) + q + str (self.b) + 'i'

    def __int__ (self):
        return int(self.a)

    def __float__ (self):
        return float(self.a)

    def __abs__ (self):
        import math
        return math.sqrt(self.a * self.a + self.b * self.b)

    def __neg__ (self):
        return Complex (-self.a, -self.b)

    def __add__ (self, other):
        if ('a' in dir(other) and 'b' in dir(other)):
            return Complex(self.a + other.a, self.b + other.b)
        else:
            return Complex(self.a + float(other), self.b)

    def __radd__ (self, other):
        if ('a' in dir(other) and 'b' in dir(other)):
            return Complex(self.a + other.a, self.b + other.b)
        else:
            return Complex(self.a + float(other), self.b)


    def __sub__ (self, other):
        return self + -other

    def __rsub__ (self, other):
        return self + -other

    def __mul__ (self, other):
        if ('a' in dir(other) and 'b' in dir(other)):
            return Complex(self.a * other.a - self.b * other.b , self.a * other.b + self.b * other.a)
        else:
            return Complex(self.a * float(other), self.b * float(other))


    def __rmul__ (self, other):
        if ('a' in dir(other) and 'b' in dir(other)):
            return Complex(self.a * other.a - self.b * other.b , self.a * other.b + self.b * other.a)
        else:
            return Complex(self.a * float(other), self.b * float(other))


    def __truediv__ (self, other):
        if ('a' in dir(other) and 'b' in dir(other)):
            return (self * other.conjugate()) / (other.a * other.a + other.b * other.b)
        else:
            return Complex(self.a / float(other), self.b / float(other))

    def __rtruediv__ (self, other):
        if ('a' in dir(other) and 'b' in dir(other)):
            return (self.conjugate() * other) / (self.a * self.a + self.b * self.b)
        else:
            return (self.conjugate()*float(other)) / (self.a * self.a + self.b * self.b)

i = Complex(0, 1)
